<header class="masthead" role="banner">
	<div class="container">
		<div class="row row-centered row-no-gutter">
			<div class="col-xs-6">
				<a class="logo-primary" href="<?php echo esc_url(home_url('/')); ?>">
					<?php bloginfo('name'); ?>
				</a>
			</div>
			<div class="col-xs-6">
				<nav class="menu-primary" role="navigation">
					<div class="container">
						<div class="row">
							<div class="col-lg-10 col-xs-12">
								<?php
									if (has_nav_menu('primary_navigation')) :
										wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'list'));
									endif;
									?>
							</div>
						</div>
					</div>
				</nav>

				<nav class="menu-secondary" role="navigation">
					<?php
					if (has_nav_menu('primary_navigation')) :
						wp_nav_menu(array('theme_location' => 'secondary_navigation', 'menu_class' => 'list'));
					endif;
					?>
				</nav>

				<button type="button" class="menu-primary-toggle">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
				</button>
			</div>
		</div>
	</div>
</header>