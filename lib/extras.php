<?php
/**
 * Clean up the_excerpt()
 */
function roots_excerpt_more($more) {
	return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'roots') . '</a>';
}
add_filter('excerpt_more', 'roots_excerpt_more');

/**
 * Manage output of wp_title()
 */
function roots_wp_title($title) {
	if (is_feed()) {
		return $title;
	}
	
	$title .= get_bloginfo('name');
	
	return $title;
}
add_filter('wp_title', 'roots_wp_title', 10);

/**
* Custom file upload extensions
*/
add_filter('upload_mimes', 'cc_mime_types');
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	$mimes['m4v'] = 'video/mp4'; 
	return $mimes;
}

/**
 * Custom Settings Pages
 */
if( function_exists('acf_add_options_page') ) {
	// add parent
	$parent = acf_add_options_page(array(
		'page_title'  => 'Site General Settings',
		'menu_title'  => 'Site Settings',
		'redirect'    => false
	));
	
	// add sub page
	acf_add_options_sub_page(array(
		'page_title'  => 'Developer',
		'menu_title'  => 'Developer',
		'parent_slug'   => $parent['menu_slug'],
	));
}

/**
* Remove admin bar
*/
add_filter('show_admin_bar', '__return_false');

/**
 * Custom Post Types
 */
/*
function create_custom_post_types() {
	register_post_type( 'service',
	array(
		'labels'            => array(
			'name'               => __( 'Services' ),
			'singular_name'      => __( 'Service' ),
			'menu_name'          => _x( 'Services', 'admin menu' ),
			'name_admin_bar'     => _x( 'Service', 'add new on admin bar'),
			'add_new'            => _x( 'Add New', 'service' ),
			'add_new_item'       => __( 'Add New Service' ),
			'new_item'           => __( 'New Service' ),
			'edit_item'          => __( 'Edit Service' ),
			'view_item'          => __( 'View Service' ),
			'all_items'          => __( 'All Services' ),
			'search_items'       => __( 'Search Services' ),
			'parent_item_colon'  => __( 'Parent Services:' ),
			'not_found'          => __( 'No services found.' ),
			'not_found_in_trash' => __( 'No services found in Trash.' )
		),
		'public'            => true,
		'show_in_menu'      => true,
		'has_archive'       => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail' ),
		'rewrite'           => array(
			'slug'       => 'services',
			'with_front' => false
		)
	);
}
add_action( 'init', 'create_custom_post_types' );
*/

/**
* Custom Taxonomies
*/
/*
add_action('init', 'create_custom_taxonomies');
function create_custom_taxonomies() {
	register_taxonomy('event_category', array('event'), array(
		'hierarchical' => true,
		'labels' => array(
			'name'              => __('Categories', ''),
			'singular_name'     => _x('category', 'taxonomy singular name', ''),
			'search_items'      => __('Search Categories', ''),
			'all_items'         => __('All Categories', ''),
			'parent_item'       => __('Parent Category', ''),
			'parent_item_colon' => __('Parent Category:', ''),
			'edit_item'         => __('Edit Category', ''),
			'update_item'       => __('Update Category', ''),
			'add_new_item'      => __('Add New Category', ''),
			'new_item_name'     => __('New Category', ''),
		),
		'show_ui' => true,
		'rewrite' => array(
			'slug'       => 'category',
			'with_front' => false,
		),
	));
}
*/