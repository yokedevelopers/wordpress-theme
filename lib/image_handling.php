<?php


/*------------------------------------------------------------------------------------------
---------------------------------- IMAGE HANDLING ------------------------------------------
-------------------------------------------------------------------------------------------*/

// THUMBNAIL IMAGE SIZES

add_action( 'after_setup_theme', 'thumb_sizes_setup' );

function thumb_sizes_setup() {
  //                Name              | Width | height | Crop
  add_image_size( "banner",              1600,   400,    true );

  add_image_size( "full_landscape",      1200,   800,    false );
  add_image_size( "full_screen",         1600,   1200,   false );

  add_image_size( "large_landscape",     720,    480,    true );
  add_image_size( "medium_landscape",    520,    240,    true );
  add_image_size( "small_landscape",     360,    240,    true );

  add_image_size( "call_to_action",      805,    535,    true );

  add_image_size( "large_portrait",      520,    624,    true );
  add_image_size( "small_portrait",      330,    220,    true );

  add_image_size( "large_square",        300,    300,    true );
  add_image_size( "small_square",        180,    180,    true );

  add_image_size( "small_logo",          250,    250,    false );

  add_image_size( "large_thumbnail",     785,    380,    true );
  add_image_size( "small_thumbnail",     360,    240,    true );

  add_image_size( "small_product",       182,    305,    true );
  add_image_size( "medium_product",      225,    382,    true );
  add_image_size( "large_product",       258,    440,    true );

  add_image_size( "content_image",       145,    115,    true );
  add_image_size( "contact_image",       140,    140,    true );
}

/*------------------------------------------------------------------------------------------
    IMAGE HELPERS
-------------------------------------------------------------------------------------------*/

function get_all_image_sizes($attachment_id = 0) {
    $sizes = get_intermediate_image_sizes();
    if(!$attachment_id) $attachment_id = get_post_thumbnail_id();

    $images = array();
    foreach ( $sizes as $size ) {
      $img_array = wp_get_attachment_image_src( $attachment_id, $size );
        $images[$size] = $img_array[0];
    }
    $img_array = wp_get_attachment_image_src( $attachment_id, 'full' );
    $images['full'] = $img_array[0];

    return $images;
}

function get_img($img_obj, $size = null) {

  if(empty($img_obj)) {
    $img_obj = get_field("default_image", 'options');
  }

  if(is_string($img_obj)):
    return $img_obj;
  endif;

  if($size == null || empty($img_obj["sizes"][$size])) {
    $return = $img_obj["url"];
  } else {
    $return = $img_obj["sizes"][$size];
  }
  return $return;
}

function get_post_img($id, $size = 'full') {
  if(is_object($id) || is_array($id)) {
    $return    = get_img($id, $size);
  }
  
  else {
    $post      = get_post($id);
    $post_type = $post->post_type;

    switch ($post_type):
      case ('post'):
      case ('page'):
      case ('project'):
          $image_id = get_post_thumbnail_id( $id );
          $src_arr  = wp_get_attachment_image_src( $image_id, $size );
          $return   = $src_arr[0];
        break;
      default:
          $return   = get_img(null, $size);
        break;
    endswitch;
  }

  if(empty($return)) {
    $return = get_img(null, $size);
  }

  if(is_ssl()):
    $src_https = str_replace('http://', 'https://', $return);
    $return = $src_https;
  endif;

  return $return;
}
?>