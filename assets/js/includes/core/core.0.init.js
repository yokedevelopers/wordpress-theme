/**
 * yoke
 * @module
 * @namespace yoke
 * @requires jquery.min.js
 * @author Angus Dowling <angusdowling@live.com.au>
 */

(function (exports) {
	/**
	 * @type {object}
	 */
	var conf = {};
	
	conf.win = {
		w: window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
		h: window.innerHeight || document.body.offsetHeight || document.documentElement.clientHeight || $(window).height()
	};

	conf.bp = {
		xs: 320,
		mb: 480,
		pb: 640,
		sm: 768,
		md: 992,
		lg: 1170
	};

	conf._flag     = false;
	conf._activeBp = null;

	conf.bpRange = {
		desktop: {
			gt: conf.bp.lg
		},

		laptop: {
			gte: conf.bp.md,
			lt: conf.bp.lg
		},

		tablet: {
			gte: conf.bp.pb,
			lt: conf.bp.md
		},

		mobile: {
			gte: 0,
			lt: conf.bp.pb
		}
	};

	exports.conf = conf;
})(this.yoke = {});